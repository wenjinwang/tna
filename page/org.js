// const MongoClient = require('mongodb').MongoClient;
// const mongoose = require('mongoose');
// const assert = require('assert');
// const path = require('path');
// const express = require('express');
// const request = require('request');
// const app = express();
var express = require('express');
var app = express();


//database
var Acadamic = { list: [
			{
				"tag": 'Acadamic',
				"name":'Alpha Epsilon Delta',
				"website": 'http://www.usc-aed.com', 
	     		"icon": 'http://www.logosc.cn/logomrdata/2017/12/21/5a23ebdc-94c0-4cfe-8693-2cd65a44c72f.png'
	    	},
	    	{
				"tag": 'Acadamic',
				"name":'3D4E - 3D Printing For Everyone',
				"website": 'https://www.3d4e.org', 
	     		"icon": 'https://static.wixstatic.com/media/d4a39c_47f02c95016a4ca280049e267ba9eab2~mv2.png/v1/fill/w_184,h_184,al_c,usm_0.66_1.00_0.01/d4a39c_47f02c95016a4ca280049e267ba9eab2~mv2.png'
	    	},
	    	{
				"name": "Advanced Periodontology Resident Study Club",
				"tag": "Academic",
				"email": "kazemies@usc.edu",
				"website": "",
				"icon": "http://www.logosc.cn/logomrdata/2017/12/21/5a23ebdc-94c0-4cfe-8693-2cd65a44c72f.png"
			},
			{
				"name": "Aerodesign Team of USC",
				"tag": "Academic",
				"email": "usc.adt.operations@gmail.com",
				"website": "",
				"icon": "http://www.logosc.cn/logomrdata/2017/12/21/5a23ebdc-94c0-4cfe-8693-2cd65a44c72f.png"
			},
			{
				"name": "African Americans in Health",
				"tag": "Academic",
				"email": "dcharles@usc.edu",
				"website": "",
				"icon": "http://www.logosc.cn/logomrdata/2017/12/21/5a23ebdc-94c0-4cfe-8693-2cd65a44c72f.png"
			}
	    	]
		};
var Cultural = { list: [
			{
				tag: 'Cultural',
				name:'Active Minds at USC',
				website: 'https://www.facebook.com/groups/USCActiveMinds/about/', 
	     		icon: 'https://www.activeminds.org/wp-content/themes/active-minds/assets/dist/img/logo.png'
	    	}]
		};
var Political = { list: [
			{
				tag: 'Political',
				name:'SC Trojans Neighborhood Association (SCTNA)',
				website: 'https://tna.usc.edu', 
	     		icon: 'https://scontent-lax3-1.xx.fbcdn.net/v/t1.0-9/28870716_531610887225494_7877108924401319936_n.jpg?_nc_cat=0&oh=5d75503825bb375f99c5b8706e8829fa&oe=5B932926'
	    	}]
		};


app.use(express.static(__dirname + '/public'));


app.get('/search', function(req, res){

	var tag = req.query.tag;

	var organizations;

	var arr = Acadamic.list.concat(Cultural.list);
	arr = arr.concat(Political.list);

	var all = {list: arr}

	if (tag == 'All'){
		organizations = all;
	}
	if (tag == 'Acadamic') {
		organizations = Acadamic;
	}
	if (tag == 'Cultural'){
		organizations = Cultural;
	}
	if (tag == 'Political'){
		organizations = Political;
	}



	res.send(organizations);
	// mongoose.connect("mongodb://localhost:27017/" + database, function(err, db) {
	// 	if (err) {
	// 		res.send("Connection Error");
	// 		throw err;
	// 	}
	// 	console.log("Connect To Database");
	// 	console.log("Search item");

	// 	var queryBuilding;
		
	// 	// Search the items
	// 	Building.find(queryBuilding, function(err, buildings) {
	// 		if (err) {
	// 			throw err;
	// 		}

	// 		if (buildings.length != 0) {
	// 			console.log(buildings);
	// 			// res.send(JSON.stringify(buildings));
	// 			res.send(buildings);
	// 			mongoose.connection.close();
	// 			console.log("Close Connection");
	// 		}
	// 	});
	// });
});

app.listen(8084);
