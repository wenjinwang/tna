
function get(tag){
  $.ajax({
      type: 'GET',
      url: 'http://tna.usc.edu/org/search',
      data: {tag: tag},
      dataType: 'json'
    })
    .done(function(data) {
      
      var size = data.list.length;
      var result = '';

      for(var i=0; i < size; i++){
          result +=
          '<li class="list-group-item">'+
            '<div class="row">'+
                '<div id="img" class="col-lg-4">'+
                  '<img src="'+ data.list[i].icon +'" class="circle">'+
                '</div>'+
                '<div id="name" class="col-lg-4">'+
                  '<a href="'+ data.list[i].website + '">' + data.list[i].name + '</a>' +  
                '</div>';

          if (tag == 'All'){
                result +=
                      '<div id="tag" class="col-lg-4">'+
                        data.list[i].tag +
                      '</div>';
          }
          result +=
            '</div>'+
          '</li>';
      }

      $('#add').html(result);  
    });
}

 